<?php

namespace Drupal\commerce_payplug\Services;

use Payplug\Notification;
use Payplug\Payment;
use Payplug\Payplug;
use Payplug\Refund;

/**
 * PayPlug encapsulation service class.
 *
 * This class can be injected in requiring class, thus allow PayPlug service
 * unit/functional testing.
 *
 * @group commerce_payplug
 */
class PayPlugService implements PayPlugServiceInterface {

  protected $api_key;

  /**
   * { @inheritdoc }
   */
  public function setApiKey($api_key) {
    $this->api_key = $api_key;
  }

  /**
   * { @inheritdoc }
   */
  public function createPayPlugPayment(array $data, Payplug $payplug = null) {
    // Set API Key
    Payplug::init([
      'secretKey' => $this->api_key,
    ]);
    $payment = Payment::create($data, $payplug);
    return $payment;
  }

  /**
   * { @inheritdoc }
   */
  public function createPayPlugRefund($payment, array $data, Payplug $payplug = null) {
    Payplug::init([
      'secretKey' => $this->api_key,
    ]);
    $refund = Refund::create($payment, $data, $payplug);
    return $refund;
  }

  /**
   * { @inheritdoc }
   */
  public function treatPayPlugNotification($notification, $authentication = null) {
    Payplug::init([
      'secretKey' => $this->api_key,
    ]);
    $resource = Notification::treat($notification, $authentication = null);
    return $resource;
  }
}
