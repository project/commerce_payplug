<?php

namespace Drupal\commerce_payplug\Services;

use Payplug\Payplug;

/**
 * Interface description for PayPlug encapsulation service class.
 *
 * This class can be injected in requiring class, thus allow PayPlug service
 * unit/functional testing.
 *
 * @group commerce_payplug
 */
interface PayPlugServiceInterface {

  /**
   * Sets the API key in PayPlug service.
   *
   * @param string $api_key
   * @return \Payplug\Payplug  the new client authentication
   */
  public function setApiKey($api_key);

  /**
   * Creates a Payment.
   *
   * @param   array $data
   *    API data for payment creation
   * @param   \Payplug\Payplug $payplug
   *    the client configuration
   *
   * @return  null|\Payplug\Resource\Payment the created payment instance
   *
   * @throws  \Payplug\Exception\ConfigurationException
   * @throws  \Payplug\Exception\ConfigurationNotSetException
   */
  public function createPayPlugPayment(array $data, Payplug $payplug = null);

  /**
   * Creates a Refund.
   *
   * @param string $payment
   *   The payment id.
   *
   * @param array $data
   *   API data for payment creation.
   *
   * @param \Payplug\Payplug $payplug
   *   the client configuration.
   *
   * @return  null|\Payplug\Refund
   *   the created refund instance.
   *
   * @throws  \Payplug\Exception\ConfigurationException
   * @throws  \Payplug\Exception\ConfigurationNotSetException
   */
  public function createPayPlugRefund($payment, array $data, Payplug $payplug = null);

  /**
   * Treats a notification received from PayPlug service.
   *
   * @param   string  $requestBody     JSON Data sent by the notifier.
   * @param   \Payplug\Payplug $authentication  The client configuration.
   *
   * @return  \Payplug\Resource\IVerifiableAPIResource  A safe API Resource.
   *
   * @throws  \Payplug\Exception\ConfigurationException
   * @throws  \Payplug\Exception\UnknownAPIResourceException
   */
  public function treatPayPlugNotification($notification, $authentication = null);

}
